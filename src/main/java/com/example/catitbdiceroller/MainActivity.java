package com.example.catitbdiceroller;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button rollButton;
    ImageView image1;
    ImageView image2;
    Button restartButton;
    int[] array ={R.drawable.dice_1,R.drawable.dice_2,R.drawable.dice_3,R.drawable.dice_4,R.drawable.dice_5,R.drawable.dice_6};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image1=findViewById(R.id.image1);
        image2=findViewById(R.id.image_2);

        rollButton = findViewById(R.id.roll_button);
        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Rolled",Toast.LENGTH_LONG).show();
                rollButton.setText("Dices Rolled");
                int num=(int) (Math.random()*6);
                int num2=(int) (Math.random()*6);
                image1.setImageResource(array[num]);
                image2.setImageResource(array[num2]);
                //todo  que  el  toast   salga arriba
//                if (num==6 && num2==6){
//                Toast toast = Toast.makeText(MainActivity.this,"JACKPÖT",Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.TOP,1000,1250);
//                toast.show();
//                }
            }
        });

        restartButton =findViewById(R.id.restart_button);
        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollButton.setText(R.string.button_text);
                image1.setImageResource(R.drawable.empty_dice);
                image2.setImageResource(R.drawable.empty_dice);
            }
        });

        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num=(int) (Math.random()*6);
                image1.setImageResource(array[num]);
            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num2=(int) (Math.random()*6);
                image2.setImageResource(array[num2]);
            }
        });

    }
}